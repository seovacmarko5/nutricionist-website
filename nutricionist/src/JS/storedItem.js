export default class Item{//pravi objekte koji cuvaju,uzimaju i brisu podatke iz sessionStorage-a
    constructor(itemType){
        this.itemType=itemType
    }
    setStoredItem(items){
        let storedItem=sessionStorage.getItem(this.itemType)
        if(!storedItem){
            sessionStorage.setItem(this.itemType,JSON.stringify(items))
            return items
        }
        sessionStorage.removeItem(this.itemType)
        sessionStorage.setItem(this.itemType,JSON.stringify(items))
        return items
    }
    getStoredItem(items){
        let storedActivities=sessionStorage.getItem(this.itemType)
        if(storedActivities){
            items=JSON.parse(storedActivities)
        }
        return items
    }
    clearStoredActivities(){
        sessionStorage.removeItem(this.itemType)
    }
}