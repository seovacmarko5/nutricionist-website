import checkSession from "./checkSession.js"
export default async function checkAdmin(userStatus,userLevel,sendToHome){
    let res=await checkSession()
    if(!res){
        userStatus(false)
        userLevel(null)
        sendToHome({name:"home"})
        return
    }
    if(res.response || res.data.res.level===2){
        userStatus(false)
        userLevel(null)
        sendToHome({name:"home"})
        return
    }
    else if(res.data.res.level===1){
        userStatus(true)
        userLevel(res.data.res.level)
    }
}