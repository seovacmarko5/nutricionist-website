import checkSession from "./checkSession.js"
export default async function checkUser(userStatus,userLevel,sendToHome){
    let res=await checkSession()
    if(!res){
        userStatus(false)
        userLevel(null)
        return
    }
    if(res.response){
        userStatus(false)
        userLevel(null)
        sendToHome({name:"home"})
        return
    }
    else if(res.data){
        userStatus(true)
        userLevel(res.data.res.level)
    }
  }